#include "image_format.h"

struct image create_img(uint32_t width, uint32_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(width * height * sizeof(struct pixel))
    };
}

void clear_img(struct image *img) {
    if (img != NULL) {
        free(img->data);
    }
}
