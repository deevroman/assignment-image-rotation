#include "bmp_utils.h"

uint8_t calculate_padding(uint32_t width) {
    if (width * sizeof(struct pixel) % 4 == 0) {
        return 0;
    } else {
        return 4 - (width * sizeof(struct pixel) % 4);
    }
}

uint8_t *create_padding_arr(uint8_t padding) {
    uint8_t *padding_arr = malloc(padding);
    if (padding_arr == NULL) {
        return NULL;
    }
    for (size_t i = 0; i < padding; i++) {
        padding_arr[i] = 0;
    }
    return padding_arr;
}

uint32_t calculate_image_size(struct image *img, uint8_t padding) {
    return (img->width * sizeof(struct pixel) + padding) * img->height;
}

