#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATIONS_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATIONS_H

#include <inttypes.h>
#include "image_format.h"

struct image rotate(struct image source);

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATIONS_H
