#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include "bmp_utils.h"
#include "image_format.h"

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_COMPRESSION,
    READ_INVALID_HEADER,
    READ_FILE_NULL,
    READ_MALLOC_ERROR,
    READ_ERROR
};

static const char * const read_status_msg[] = {
        [READ_OK] = "File read successfully",
        [READ_INVALID_SIGNATURE] = "Header file type is not supported",
        [READ_INVALID_BITS] = "This color depth is not supported.",
        [READ_INVALID_COMPRESSION] = "Compression is not supported.",
        [READ_INVALID_HEADER] = "The read title is incorrect",
        [READ_FILE_NULL] = "File NULL passed for reading",
        [READ_MALLOC_ERROR] = "Failed to allocate memory to store the image",
        [READ_ERROR] = "Error while reading the file",
};

enum write_status {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_ERROR,
    WRITE_FILE_NULL
};

static const char * const write_status_msg[] = {
        [WRITE_OK] = "File written successfully",
        [WRITE_HEADER_ERROR] = "Header entry error",
        [WRITE_ERROR] = "An error occurred while writing the file",
        [WRITE_FILE_NULL] = "NULL file passed for writing"
};

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, struct image *img);


#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
