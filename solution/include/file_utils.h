#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H

#include <stdio.h>
#include <inttypes.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR_NULL_FILE_PATH,
    OPEN_ERROR
};

static const char * const open_status_msg[] = {
        [OPEN_OK] = "File opened successfully",
        [OPEN_ERROR_NULL_FILE_PATH] = "A NULL path was passed to open the file",
        [OPEN_ERROR] = "An error occured while opening the file"
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_NULL_FILE,
    CLOSE_ERROR
};

static const char *const close_status_msg[] = {
        [CLOSE_OK] = "File closed successfully",
        [CLOSE_NULL_FILE] = "A NULL path was passed to close the file",
        [CLOSE_ERROR] = "An error occured while closing the file"
};

enum open_status open_file(FILE **file, const char *file_path, const char *mode);

enum close_status close_file(FILE *file);


#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_UTILS_H
