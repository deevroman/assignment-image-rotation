#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_UTILS_H

#include <stdlib.h>
#include <inttypes.h>
#include "image_format.h"

uint8_t calculate_padding(uint32_t width);

uint8_t *create_padding_arr(uint8_t padding);

uint32_t calculate_image_size(struct image *img, uint8_t padding);


#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_UTILS_H
